# fullstack

Fullstack Wiki

Descriptif général & fonctionnalité

Cette application va permettre, l’ajout, la modification et la suppression des articles. Il sera aussi possible de trouver un article grâce à son titre et de modifier ce dernier. Dans un souci de temps (lié à nos activités en entreprise) la partie « versionning » et tag n’ont pu être traitées.
Tout d’abord on arrive sur notre page d’accueil qui contient une barre de recherche pour trouver un article (à l’aide de son titre). En cliquant sur l’article, on peut voir son statut (publié ou non) et la description de ce dernier. Le bouton Editer permet de modifier l’article.
Ensuite pour ajouter un article, il suffit d’aller dans l’onglet Ajouté et de remplir le «formulaire».

Manuel d’installation

Partie BackOffice (en node.js)
1. Nous avons besoin d’installer MongoDB : npm install MongoDB afin de faire la connexion avec la BDD de Mongo.
2. Nous avons besoin d’installer Cors : npm install cors
3. Nous avons besoin d’installer Express : npm install Express
4. Afin de lancer le serveur : node server1.js

Partie FrontOffice (en React)
1. Nous avons besoin d’installer Bootsrap : npm install bootstrap
2. Nous avons besoin d’installer React Router… : npm install react-router-dom
3. Nous avons besoin d’installer Axios : npm install axios
4. Et enfin il faudra lancer l’application via : npm start

Manuel de Git

URL du Git : https://gitlab.com/anthony.stln/fullstack
Créer un dossier que vous appellerez « mern-crud »
Dedans faites un git clone https://gitlab.com/anthony.stln/fullstack.git
Faites un git checkout backOffice pour récupérer le contenu de la branche backOffice.
Faites un git checkout frontOffice pour récupérer le contenu de la branche frontOffice.

Manuel de MongoDB

Afin d’avoir une database correspondante avec le backoffice (nodeJS) veuillez créer une database dans MongoDB.
Veuillez créer une database appelée, « wiki » et dedans vous créerez une table « articles ».